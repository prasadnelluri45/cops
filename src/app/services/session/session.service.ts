import { Injectable, Inject } from '@angular/core';
import { APP_CONSTANTS } from './../../constants/app-contants';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  storePrivateData(data: any) {
    this.storage.setItem("PRIVATEDATA", JSON.stringify(data));
  }
  getPrivateData() {
    return JSON.parse(this.storage.getItem("PRIVATEDATA"));
  }
  private STORE_KEY: String = APP_CONSTANTS.userSessionKey;
  private STORE_TOKEN_KEY: String = APP_CONSTANTS.userTokenKey;
  private storage:any = window.localStorage;
  constructor() { }
  public getUserName() {
    const { firstName = '', lastName = '' } = this.getSessionDetails();
    return `${firstName} ${lastName}`;
  }
  public getUserNameWithUnderScore() {
    const { firstName = '', lastName = '' } = this.getSessionDetails();
    return `${firstName}_${lastName}`;
  }
  public getUserPick() {
    const { profileImage = '' } = this.getSessionDetails();
    return profileImage;
  }
  public getUserID(): String {
    const { associateId = '', id = '' } = this.getSessionDetails();
    return id;
  }
  public getUserProfilePic(): String {
    const { profile_url = '', id = '' } = this.getSessionDetails();
    return profile_url;
  }

  public getToken(): String {
    let access_token = this.getTokenDetails();
    return access_token;
  }
  public isTokenAvaialable() {
    let access_token  = this.getTokenDetails();
    return access_token !== null;
  }
  public isUserLoggerIn(): boolean {
    const uSessionDetails = this.getSessionDetails();
    if (uSessionDetails !== null) {
      return true;
    }
    return false;
  }
  public storeAuthToken(data: any) {
    this.storage.setItem(this.STORE_TOKEN_KEY.toString(), data);
  }
  public getTokenDetails() {
    return (this.storage.getItem(this.STORE_TOKEN_KEY.toString())) || {};
  }
  public getBearerToeken() {
    return `Bearer ${this.getToken()}`;
  }
  public getBasicToeken() {
    return `Basic ${this.getToken()}`;
  }
  public storeSessionDetails(data: Object) {
    this.storage.setItem(this.STORE_KEY.toString(), JSON.stringify(data));
  }

  public storeOTPDetails(data: Object) {
    this.storage.setItem("TTPDetails", JSON.stringify(data));
  }
  public getOTPDetails() {
    return JSON.parse(this.storage.getItem("TTPDetails")) || null;
  }
  public getSessionDetails() {
    return JSON.parse(this.storage.getItem(this.STORE_KEY.toString())) || null;
  }
  public logOut() {
    localStorage.clear();
    sessionStorage.clear();
    // this.storage.remove(this.STORE_KEY.toString());
    // this.storage.remove(this.STORE_TOKEN_KEY.toString());    
  }

}
