import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from '../../utils/http/http.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
 
  constructor(private _httpService:HttpService) { }

  login(data:any):Observable<any>{
    return  this._httpService.doCall("POST","login",data,{});
  } 
  reset(data:any):Observable<any>{
    return  this._httpService.doCall("POST","forget",data,{});
  } 
  varifypasscode(data:any):Observable<any>{
    return  this._httpService.doCall("POST","varifypasscode",data,{});
  } 
  resetotp(data:any):Observable<any>{
    return  this._httpService.doCall("POST","resetotp",data,{});
  } 
  setpassword(data:any):Observable<any>{
    return  this._httpService.doCall("PUT","setpassword",data,{});
  } 

  
}
