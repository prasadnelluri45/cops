import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from 'src/app/utils/http/http.service';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private _httpService: HttpService) { }

  getfitnesscategory(): Observable<any> {
    return this._httpService.doCall("GET", "getfitnesscategory", {}, {});
  }
  getfitnesscategorycoach(data: any): Observable<any> {
    return this._httpService.doCall("POST", "getfitnesscategorycoach", data, {});
  }
  getlanguages(): Observable<any> {
    return this._httpService.doCall("GET", "getlanguages", {}, {});
  }
  
  pendingcoach(data: any): Observable<any> {
    return this._httpService.doCall("POST", "pendingcoach", data, {});
  }
  coachstatus(data: any): Observable<any> {
    return this._httpService.doCall("PUT", "coachstatus", data, {});
  }
  fitnesscategorypackage_post(data: any): Observable<any> {
    return this._httpService.doCall("POST", "fitnesscategorypackage", data, {});
  }
  fitnesscategorypackage_put(data: any): Observable<any> {
    return this._httpService.doCall("PUT", "fitnesscategorypackage", data, {});
  }
  fitnesscategorypackage_delete(id: any): Observable<any> {
    return this._httpService.doCall("DELETE", "fitnesscategorypackage/" + id, {}, {});
  }
  coachprofiledata(data: any): Observable<any> {
    return this._httpService.doCall("POST", "coachprofiledata", data, {});
  }
  profilepasswordupdate(data: any): Observable<any> {
    return this._httpService.doCall("PUT", "profilepasswordupdate", data, {});
  }
  profileupdate(data: any): Observable<any> {
    return this._httpService.doCall("PUT", "profileupdate", data, {});
  }
  
  








}
