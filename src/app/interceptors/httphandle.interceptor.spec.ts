import { TestBed } from '@angular/core/testing';

import { HttphandleInterceptor } from './httphandle.interceptor';

describe('HttphandleInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      HttphandleInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: HttphandleInterceptor = TestBed.inject(HttphandleInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
