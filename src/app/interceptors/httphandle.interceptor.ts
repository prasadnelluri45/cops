import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpResponse
} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from "rxjs/operators";
@Injectable()
export class HttphandleInterceptor implements HttpInterceptor {

  constructor() { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    // request = request.clone({
    //   headers: request.headers.set('token', "token")
    // });
    return next.handle(request).pipe(
      tap(evt => {
        if (evt instanceof HttpResponse) {

        }
      }),
      catchError((err: any) => {
        if (err instanceof HttpErrorResponse) {

        }
        return of(err);
      }));
  }
}
