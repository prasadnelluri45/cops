import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { HttpService } from 'src/app/utils/http/http.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { AlertService } from 'src/app/services/alert/alert.service';
import { SessionService } from 'src/app/services/session/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public userName = 'sorathiadaksh@gmail.com';
  public password = 'SM1234';
  public isShowPassword = false;
  private deviceInfo: any = {};
  constructor(private _authService: AuthService,
    private deviceService: DeviceDetectorService,
    private alertService: AlertService,
    private session:SessionService,
    private router:Router) { }

  ngOnInit(): void {
    this.session.logOut();
    this.deviceInfo = this.deviceService.getDeviceInfo();
  }

  clickOnEye(){
    this.isShowPassword = !this.isShowPassword;
  }

  doLogin(){
    const payload = {
      "is_social_login":false,
      "social_id":"",
      "full_name":"",
      "email_address":this.userName,
      "password":this.password,
      "device_token":"",
      "device_type":"I",
      "login_type":"Manual",
      "app_version":"1.0.0",
      "ip_address":"",
      "device_info": this.deviceInfo
  }
    this._authService.login(payload).subscribe((res: any) => {
      if(res.status === false){
        this.alertService.error("Error",res.message);
        return;       
      }
      if(res.status === true){
        this.session.storeSessionDetails(res.data);
        this.session.storeAuthToken(res.access_token);
        this.router.navigate(['dashboard']);
      }

    }, (err: any) => {
      
    })
  }

}
