import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/services/app-services/app.service';
import { AlertService } from 'src/app/services/alert/alert.service';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public display = false;
  public enrollUserList: any = ""
  public Categeories: any = [];
  public totalUsers: any = 0;
  public selectedCat = '0';
  public requests: any = 0;
  public requestsList: any = [];
  public isUserListLoading = false;
  public languages: any = [];
  public isPendUserListLoading: boolean = false;
  public genders: any = [{
    name: "MALE",
    id: 1
  }, {
    name: "FEMALE",
    id: 2
  }, {
    name: "OTHERS",
    id: 3
  }];
  public levels: any = [{ name: "Basic", level: 1 },
  { name: "Standard", level: 2 },
  { name: "Advanced", level: 3 },
  { name: "Proficient", level: 4 },
  { name: "Expert", level: 5 }]
  public ratingVal: any = 5;
  constructor(private services: AppService,
    private router: Router,
    private confirmationService: ConfirmationService,
    private alertService: AlertService,) { }

  ngOnInit(): void {
    this.getfitnesscategory();
    this.getfitnesscategorycoach({});
    this.pendingcoach();
    this.getlanguages();
  }
  goToDetails(p: any) {
    this.router.navigate(['coach-profile/coach/' + p.user_id])
  }
  goToDetails1(p: any) {
    this.router.navigate(['coach-profile/rcoach/' + p.user_id])
  }
  onSearchFn(data: any) {
    console.log(data)
  }

  getfitnesscategory() {
    this.services.getfitnesscategory().subscribe((res: any) => {
      if (res.status === true) {
        this.Categeories = res.data || [];
      }
    }, (err: any) => {

    })
  }
  catClick(c: any) {
    this.selectedCat = c.fitness_category_id;
    this.getfitnesscategorycoach({ fitness_category_id: c.fitness_category_id })
    console.log(c)
  }
  getlanguages() {
    this.services.getlanguages().subscribe((res: any) => {
      if (res.status === true) {
        this.languages = res.data;
      }
    }, (err: any) => {

    })
  }
  filterClick(type: any, i: any) {
    if (type == "G") {
      this.genders.forEach((ele: any, ind: any) => ind !== i ? ele.isSelected = false : '');
      this.genders[i]['isSelected'] = !this.genders[i]['isSelected'];
    }
    if (type == "L") {
      this.levels.forEach((ele: any, ind: any) => ind !== i ? ele.isSelected = false : '');
      this.levels[i]['isSelected'] = !this.levels[i]['isSelected'];
    }
    if (type == "LG") {
      this.languages.forEach((ele: any, ind: any) => ind !== i ? ele.isSelected = false : '');
      this.languages[i]['isSelected'] = !this.languages[i]['isSelected'];
    }
  }
  getfitnesscategorycoach(data: any) {
    this.isUserListLoading = true;
    this.totalUsers = 0;
    this.enrollUserList = [];
    const payload = {
      "fitness_category_id": this.selectedCat,
      "PageRecord": 10,
      "PageNo": 1,
      "gender": "0",
      "experience_level": "0",
      "user_languages": [],
      "rating": 0,
      "search": "",
      ...data
    }
    this.services.getfitnesscategorycoach(payload).subscribe((res: any) => {
      this.isUserListLoading = false;
      if (res.status === true) {
        this.totalUsers = res.data.count;
        this.enrollUserList = res.data.rows || [];
      }
    }, (err: any) => {

    })
  }

  pendingcoach() {
    this.isPendUserListLoading = true;
    this.requests = 0;
    this.requestsList = [];
    const payload = {
      "PageRecord": 10,
      "PageNo": 1
    }
    this.services.pendingcoach(payload).subscribe((res: any) => {
      this.isPendUserListLoading = false;
      if (res.status === true) {
        this.requests = res.data.count;
        this.requestsList = res.data.rows || [];
      }
    }, (err: any) => {

    })
  }

  scroll(id:any) {
    console.log(`scrolling to ${id}`);
    let el:any = document.getElementById(id);
    el.scrollIntoView();
  }
  coachstatus(id: any, status: any) {
    
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      accept: () => {
        this.requests = 0;
        this.requestsList = [];
        const payload = {
          "coach_profile_id": id,
          "status": status
        }
        this.services.coachstatus(payload).subscribe((res: any) => {
          if (res.status === true) {
            this.alertService.success("Success", res.message);
            this.pendingcoach();
            this.getfitnesscategorycoach({})
          } else {
            this.alertService.error("Success", res.message);
          }
        }, (err: any) => {

        })
      }
    });
  }
  applyFilter() {
    const gender = this.genders.find((ele: any) => ele.isSelected);
    const level = this.levels.find((ele: any) => ele.isSelected);
    const lang = this.languages.find((ele: any) => ele.isSelected);
    const payload = {
      "fitness_category_id": this.selectedCat,
      "PageRecord": 10,
      "PageNo": 1,
      "gender": "0",
      "experience_level": "0",
      "user_languages": [],
      "rating": 0,
      "search": ""
    }
    if (gender) {
      payload.gender = gender.id;
    }
    if (level) {
      payload.experience_level = level.level;
    }
    if (lang) {
      payload.user_languages = this.languages.filter((ele: any) => ele.isSelected);
    }
    if (this.ratingVal) {
      payload.rating = this.ratingVal;
    }
    this.getfitnesscategorycoach(payload);
  }
  getCatName(c: any) {
    try {
      if (c.user_fitness_categories.length === 0) return "No category"
      return c.user_fitness_categories.map((a: any) => a.title).join(", ")
    } catch (ex) { }
    return "";
  }
  openFilter() {
    this.display = true;
  }
}
