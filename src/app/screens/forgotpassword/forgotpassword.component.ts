import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { AuthService } from 'src/app/services/auth/auth.service';
import { AlertService } from 'src/app/services/alert/alert.service';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.scss']
})
export class ForgotpasswordComponent implements OnInit {
  public email: string = "csekharui@gmail.com";
  constructor(private services: AuthService,
    private router: Router,
    private _location: Location,
    private alertService: AlertService,
    private sessionn: SessionService) { }

  ngOnInit(): void {
  }
  goBack() {
    this._location.back();
  }

  doClick() {
    const payload = {
      "email_address": this.email
    }
    this.services.reset(payload).subscribe((res: any) => {
      if (res.status === false) {
        this.alertService.error("Error", res.message);
        return;
      }
      if (res.status === true) {
        this.alertService.success("Success", res.message);
        this.sessionn.storeOTPDetails({ ...res.data, user_email: this.email });
        this.router.navigate(['OTP']);
      }

    }, (err: any) => {

    })
  }

}
