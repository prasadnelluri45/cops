import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { SessionService } from '../../services/session/session.service';
import { Observable } from 'rxjs';
import { APP_CONSTANTS } from './../../constants/app-contants';


@Injectable({
  providedIn: 'root'
})
export class HttpService {
  constructor(private _http: HttpClient,
    private _session: SessionService) {

  }
  getAuthHeaders() {
    return {
      headers: {
        'Content-Type': 'application/json',
        'authorization': `${this._session.getBearerToeken()}`,
        // 'user_id': " this._session.getUserID()"
      }
    }
  }
  getAuthHeadersFileUpload() {
    return {
      headers: {
        'Toekn': `${this._session.getToken()}`,
        'user_id': " this._session.getUserID()"
      }
    }
  }

  getHeaders() {
    return {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      }
    }
  }

  doCall(type: string, url: string, payload: any, headers: any): Observable<any> {
    url = `${APP_CONSTANTS.apiHost}${url}`;
    headers = this.getAuthHeaders();
    switch (type) {
      case "GET":
        return this.getCall(url, headers);
        break;
      case "POST":
        return this.postCall(url, payload, headers);
        break;
      case "DELETE":
        return this.deleteCall(url, payload, headers);
        break;
      case "PUT":
        return this.putCall(url, payload, headers);
        break;
    }
    return this.getCall(url, headers);
  }

  postCall(url: string, payload: any, headers: any): Observable<any> {
    return new Observable(subscriber => {
      this._http.post(url, payload, headers).subscribe((res: any) => {
        subscriber.next(res);
        subscriber.complete();
      }, (e: any) => {
        const { error, status } = e;
        subscriber.error(e);
        subscriber.complete();
      })
    });
  }
  getCall(url: string, headers: any): Observable<any> {
    return new Observable(subscriber => {
      this._http.get(url, headers).subscribe((res: any) => {
        subscriber.next(res);
        subscriber.complete();
      }, (e: any) => {
        const { error, status } = e;
        subscriber.error(e);
        subscriber.complete();
      })
    });
  }
  putCall(url: string, payload: any, headers: any): Observable<any> {
    return new Observable(subscriber => {
      this._http.put(url, payload, headers).subscribe((res: any) => {
        subscriber.next(res);
        subscriber.complete();
      }, (e: any) => {
        const { error, status } = e;
        subscriber.error(e);
        subscriber.complete();
      })
    });
  }

  deleteCall(url: string, payload: any, headers: any): Observable<any> {
    return new Observable(subscriber => {
      this._http.delete(url,  headers).subscribe((res: any) => {
        subscriber.next(res);
        subscriber.complete();
      }, (e: any) => {
        const { error, status } = e;
        subscriber.error(e);
        subscriber.complete();
      })
    });
  }
}
