import { Component, OnInit,Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
@Output() onSearchFn:any = new EventEmitter();
@Output() openFilter:any = new EventEmitter();
public isShow = false;
  constructor() { }

  ngOnInit(): void {
  }
  showOptions(){
    this.isShow = !this.isShow;
  }
  onSearch(e:any){
 if(e.keyCode == 13){
   this.onSearchFn.emit({data:"Thanks Lord"})
 }
  }
  onFilterClick(){
    this.openFilter.emit({data:"Thanks Lord"})
  }

}
