import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './../screens/login/login.component';
import { DashboardComponent } from './../screens/dashboard/dashboard.component';
import { FeedsComponent } from '../screens/feeds/feeds.component';


import { AuthGuard } from './../guards/auth/auth.guard';



const routes: Routes = [{
  path: "",
  component: LoginComponent,
  canActivate: []
},
{
  path: "login",
  component: LoginComponent,
  canActivate: []
},
{
  path:"feeds",
  component:FeedsComponent,
  canActivate:[]
}
// }, {
//   path: "forgot",
//   component: ForgotpasswordComponent,
//   canActivate: []
// }, {
//   path: "OTP",
//   component: VerifyOTPComponent,
//   canActivate: []
// }, {
//   path: "newpassword",
//   component: CreateNewPasswordComponent,
//   canActivate: []
// }, {
//   path: "dashboard",
//   component: ComingSoonComponent,
//   canActivate: [AuthGuard]
// }, {
//   path: "coach",
//   component: DashboardComponent,
//   canActivate: [AuthGuard]
// }, {
//   path: "plans",
//   component: PlansComponent,
//   canActivate: [AuthGuard]
// }, {
//   path: "add-plan",
//   component: AddaplanComponent,
//   canActivate: [AuthGuard]
// }, {
//   path: "plan-details",
//   component: PlansDetailsComponent,
//   canActivate: [AuthGuard]
// }, {
//   path: "coming-soon",
//   component: ComingSoonComponent,
//   canActivate: [AuthGuard]
// }, {
//   path: "coach-profile/:type/:uid",
//   component: CoachProfileInfoComponent,
//   canActivate: [AuthGuard]
// }, {
//   path: "profile",
//   component: ProfileDetailsComponent,
//   canActivate: [AuthGuard]
//}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
