import { Component } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'COPS';
  
  
  public currentRout: any = "";

  public loginPage:boolean=true;
  public homePage:boolean=false;

  constructor( private router: Router) {  
    this.router.events.subscribe((res) => { 
      console.log(this.currentRout=this.router.url);
        if((this.currentRout==="/") || (this.currentRout==='/login')){
          this.loginPage = true;
          this.homePage=false;
        }else{
          this.loginPage = false;
          this.homePage=true;
        }
    });

     }

    

    
    

}
