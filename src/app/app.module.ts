import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './routes/app-routing.module';
import { AppComponent } from './shell/app.component';
import { LoginComponent } from './screens/login/login.component';
import { DashboardComponent } from './screens/dashboard/dashboard.component';
import { SessionService } from './services/session/session.service';
import { AuthGuard } from './guards/auth/auth.guard';
import { HttphandleInterceptor } from './interceptors/httphandle.interceptor';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import {ToastModule} from 'primeng/toast';
import {MessageService} from 'primeng/api';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { AddaplanComponent } from './screens/addaplan/addaplan.component';
// import { CoachProfileInfoComponent } from './screens/coach-profile-info/coach-profile-info.component';
// import { ProfileDetailsComponent } from './screens/profile-details/profile-details.component';
// import { SuperAdminCoachComponent } from './screens/super-admin-coach/super-admin-coach.component';
// import { ForgotpasswordComponent } from './screens/forgotpassword/forgotpassword.component';
// import { VerifyOTPComponent } from './screens/verify-otp/verify-otp.component';
// import { CreateNewPasswordComponent } from './screens/create-new-password/create-new-password.component';
// import { PlansComponent } from './screens/plans/plans.component';
// import { PlansDetailsComponent } from './screens/plans-details/plans-details.component';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {DialogModule} from 'primeng/dialog';
import {RatingModule} from 'primeng/rating';
import { ConfirmationService } from 'primeng/api';
// import { ComingSoonComponent } from './screens/coming-soon/coming-soon.component';
import { TopnavComponent } from './components/topnav/topnav.component';
import { MainmenuComponent } from './components/mainmenu/mainmenu.component';
import { NewsfeedComponent } from './screens/newsfeed/newsfeed.component';
import { FeedsComponent } from './screens/feeds/feeds.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    HeaderComponent,
    FooterComponent,
    SidenavComponent,
    TopnavComponent,
    MainmenuComponent,
    NewsfeedComponent,
    FeedsComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ToastModule,    
    ConfirmDialogModule,
    ProgressSpinnerModule,
    DialogModule,
    RatingModule
  ],
  providers: [
    ConfirmationService,
    MessageService,
    { provide: HTTP_INTERCEPTORS, useClass: HttphandleInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
